lvcreate -L 15G --thinpool thin_pool uni

- l100%FREE:
lvcreate -l100%FREE --thinpool thin_pool uni

# Creating Thin Volumes
lvcreate -V 5G --thin -n thin_vol_client1 vg_thin/tp_tecmint_pool

# Raid Thin Volumes
lvcreate -V 5G --thin -n thin_vol_client1 vg_thin/tp_tecmint_pool

lvcreate -i2 -I16 -V 5G --thin -n thin_raid_vol uni/thin_pool

lvcreate -i2 -I16 -l100%FREE -nraid0 uni
